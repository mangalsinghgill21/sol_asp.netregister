﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CustomerPage.aspx.cs" Inherits="Soln_Asp.NetRegister.CustomerPage" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="Css/css.css" type="text/css" rel="stylesheet" />
</head>
<body>
    <form id="form1" runat="server">
    <div style="width: 800px; text-align: center;">
     <table style="width: 140%;">
         <tr>
             <td>
                 <asp:TextBox ID="txtfirstname" runat="server" CssClass="txt" placeholder="Firstname" />
             </td>
         </tr>
         <tr>
             <td>
                 <asp:TextBox ID="txtlastname" runat="server" CssClass="txt" placeholder="Lastname" />
             </td>
         </tr>
         <tr>
             <td>
                 <asp:TextBox ID="txtcity" runat="server" CssClass="txt" placeholder="City" />
             </td>
         </tr>
         <tr>
             <td>
                 <asp:TextBox ID="txtstate" runat="server" CssClass="txt" placeholder="state"/>
            <br />
                  </td>
         </tr>
         <tr>
             <td><br />
                <asp:Button ID="btnsubmit" runat="server" CssClass="btn btn2" OnClick="btnsubmit_Click" />
             </td>
         </tr>

     </table>
    </div>
    </form>
</body>
</html>
