﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="EmployeePage.aspx.cs" Inherits="Soln_Asp.NetRegister.EmployeePage" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="Css/css.css" type="text/css" rel="stylesheet" />
</head>
<body>
    <form id="form1" runat="server">
    <div  style="width: 800px; text-align: center;">
        <table style="width: 140%;">
            <tr>
                <td>
                    <asp:TextBox ID="txtemail" runat="server" CssClass="txt" placeholder="Emailid" />           
                </td>
            </tr>
             <tr>
                <td>
                    <asp:TextBox ID="txtpassword" runat="server" CssClass="txt" placeholder="Password" />             
                </td>
            </tr>
             <tr>
                <td>
                <asp:Button ID="btnsubmit" runat="server" CssClass="btn btn2" OnClick="btnsubmit_Click" />
             
                </td>
            </tr>
        </table>
    
    </div>
    </form>
</body>
</html>
