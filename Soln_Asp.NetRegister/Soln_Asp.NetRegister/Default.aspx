﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="Soln_Asp.NetRegister.Default" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Task </title>
    <link href="Css/css.css" type="text/css" rel="stylesheet" />
</head>
<body>
    <form id="form1" runat="server">
    <div style="width: 800px; text-align: center;">
    <table style="width: 140%;">
        <tr>
            <td style ="text-align:center">
                <span>Register as Customer</span><br />
                <asp:Button ID="btncust" runat="server" Text="Customer" CssClass="btn btn2" OnClick="btncust_Click" />
            </td>
        </tr>
        <tr>
            <td><br /></td>
        </tr>
        <tr>
            <td style ="text-align:center">
                <span>Register as Employee</span> <br />
                <asp:Button ID="btnemp" runat="server" Text="Emp" CssClass="btn btn2" OnClick="btnemp_Click" />
            </td>
        </tr>
    </table>
    </div>
    </form>
</body>
</html>
