﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Soln_Asp.NetRegister
{
    public partial class Default : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
              
        }

        protected void btncust_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/CustomerPage.aspx");
        }

        protected void btnemp_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/EmployeePage.aspx");
        }
    }
}