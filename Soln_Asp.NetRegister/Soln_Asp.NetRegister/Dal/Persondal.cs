﻿using Soln_Asp.NetRegister.Dal.Interface;
using Soln_Asp.NetRegister.Entity.Interface;
using Soln_Asp.NetRegister.ORM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;


namespace Soln_Asp.NetRegister.Dal
{
    public class Persondal : Ipersondal
    {
        #region Declaration
        private RegisterdcDataContext dc = null;
        #endregion

        #region Constructor
        public Persondal()
        {
            dc = new RegisterdcDataContext();
        }       
        #endregion

        public async  Task<bool> Insert(IPersonEntity entityobj)
        {
            int? status = null;
            String message = null;
            
            return await Task.Run(() => 
            {

                var setQuery =
                    dc
                    .uspsetcust(
                        "Insert"
                        ,entityobj.custentityobj.custid
                        ,entityobj.custentityobj.firstname
                        ,entityobj.custentityobj.lastname
                        ,entityobj.custentityobj.city
                        ,entityobj.custentityobj.state
                        ,entityobj.empentityobj.empid
                        ,entityobj.empentityobj.emailid
                        ,entityobj.empentityobj.password
                        ,ref status
                        ,ref message
                    );
             
            
                return (status == 1)?true : false;

            });

        
        }
  
    }
}
