﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Soln_Asp.NetRegister.Entity.Interface
{
   public interface Iempentity
    {
         dynamic empid { get; set; }
         dynamic emailid { get; set; }
         dynamic password { get; set; }
    }
}
