﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Soln_Asp.NetRegister.Entity.Interface
{
    public interface IPersonEntity
    {
        customerentity custentityobj { get; set; }
        empentity empentityobj { get; set; }
    }
}
