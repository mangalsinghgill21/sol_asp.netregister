﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Soln_Asp.NetRegister.Entity.Interface
{
  public interface Icustentity
    {
        dynamic custid { get; set; }
        dynamic firstname { get; set; }
        dynamic lastname { get; set; }
        dynamic city { get; set; }
       dynamic state { get; set; }

    }
}
