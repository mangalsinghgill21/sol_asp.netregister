﻿using Soln_Asp.NetRegister.Entity.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Soln_Asp.NetRegister.Entity
{
    public class customerentity : Icustentity
    {
        public dynamic custid { get; set; }
        public dynamic firstname { get; set; }
        public dynamic lastname { get; set; }
        public dynamic city { get; set; }
        public dynamic state { get; set; }

    }
}