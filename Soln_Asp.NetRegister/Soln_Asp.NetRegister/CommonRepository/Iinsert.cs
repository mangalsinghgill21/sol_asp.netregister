﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Soln_Asp.NetRegister.CommonRepository
{
    public interface Iinsert<TEntity> where TEntity : class
    {
        Task<Boolean> Insert(TEntity entityObj);
    }
}
