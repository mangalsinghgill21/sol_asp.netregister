﻿CREATE TABLE [dbo].[tblemp] (
    [Empid]    NUMERIC (18)   NOT NULL IDENTITY,
    [emailid]  NVARCHAR (50)  NOT NULL,
    [password] NVARCHAR (MAX) NOT NULL,
    CONSTRAINT [PK_tblemp] PRIMARY KEY CLUSTERED ([Empid] ASC)
);

