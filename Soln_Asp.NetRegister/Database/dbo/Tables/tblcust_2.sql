﻿CREATE TABLE [dbo].[tblcust] (
    [Custid]    NUMERIC (18)   NOT NULL IDENTITY,
    [firstname] NVARCHAR (MAX) NOT NULL,
    [lastname]  NVARCHAR (MAX) NOT NULL,
    [city]      NVARCHAR (MAX) NOT NULL,
    [state]     NVARCHAR (50)  NOT NULL,
    CONSTRAINT [PK_tblcust] PRIMARY KEY CLUSTERED ([Custid] ASC)
);

