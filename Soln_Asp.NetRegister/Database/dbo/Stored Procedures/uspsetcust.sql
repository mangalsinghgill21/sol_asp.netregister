﻿CREATE PROCEDURE [dbo].[uspsetcust]
	@Command nVarchar(50)=NULL,
	
	@custid Numeric(18,0)=NULL,
	@firstname nVarchar(50)=NULL,
	@lastname nVarchar(50)=NULL,
	@city nVarchar(50)=NULL,
	@state nVarchar(50)=NULL
	 
AS
	BEGIN 
		
		DECLARE @ErrorMessage varchar(MAX)

			IF @Command='Insert'
				BEGIN
					
					BEGIN TRANSACTION

					BEGIN TRY 
						
						INSERT INTO tblcust
						(
							Custid,
							firstname,
							lastname,
							city,
							state
						)
						VALUES
						(
							@custid,
							@firstname,
							@lastname,
							@city,
							@state
						)

						

						COMMIT TRANSACTION
					END TRY 

					BEGIN CATCH 
						SET @ErrorMessage=ERROR_MESSAGE()
						ROLLBACK TRANSACTION
						RAISERROR(@ErrorMessage,16,1)
					END CATCH

				END			
	END 
