﻿CREATE PROCEDURE [dbo].[uspsetemp]
	@Command nVarchar(50)=NULL,
	
	@empid Numeric(18,0)=NULL,
	@emailid nVarchar(50)=NULL,
	@password nVarchar(50)=NULL
	 
AS
	BEGIN 
		
		DECLARE @ErrorMessage varchar(MAX)

			IF @Command='Insert'
				BEGIN
					
					BEGIN TRANSACTION

					BEGIN TRY 
						
						INSERT INTO tblemp
						(
						    Empid,
							emailid,
							password
						)
						VALUES
						(
							@empid,
							@emailid,
							@password
						)

						

						COMMIT TRANSACTION
					END TRY 

					BEGIN CATCH 
						SET @ErrorMessage=ERROR_MESSAGE()
						ROLLBACK TRANSACTION
						RAISERROR(@ErrorMessage,16,1)
					END CATCH

				END			
	END 
