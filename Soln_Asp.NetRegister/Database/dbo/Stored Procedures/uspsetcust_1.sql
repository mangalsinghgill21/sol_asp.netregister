﻿CREATE PROCEDURE [dbo].[uspsetcust]
	@Command nVarchar(50)=NULL,
	
	@custid Numeric(18,0)=NULL,
	@firstname nVarchar(50)=NULL,
	@lastname nVarchar(50)=NULL,
	@city nVarchar(50)=NULL,
	@state nVarchar(50)=NULL,
	@empid Numeric(18,0)=NULL,
	@emailid nVarchar(50)=NULL,
	@password nVarchar(50)=NULL,
	@Status int=NULL OUT,
	@Message Varchar(MAX)=NULL OUT

	 
AS
	BEGIN 
		
		DECLARE @ErrorMessage varchar(MAX)

			IF @Command='Insert'
				BEGIN
					
					BEGIN TRANSACTION

					BEGIN TRY 
						
						INSERT INTO tblcust
						(
							Custid,
							firstname,
							lastname,
							city,
							state
						)
						VALUES
						(
							@custid,
							@firstname,
							@lastname,
							@city,
							@state
						)
					
		EXEC uspsetemp @Command,@empid,@emailid,@password


						SET @Status=1
						SET @Message='Insert Succesfully'
					
						COMMIT TRANSACTION
					END TRY 

					BEGIN CATCH 
						SET @ErrorMessage=ERROR_MESSAGE()
						ROLLBACK TRANSACTION

						SET @Status=0
						SET @Message='Insert Exception'

						RAISERROR(@ErrorMessage,16,1)
					END CATCH

				END			
	END 
