﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Soln_Asp.NetRegister.Dal;
using Soln_Asp.NetRegister.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UnitTest1
{
    class registerunittest
    {
        public void TestMethod()
        {
            Task.Run(async () =>
            {

                var personentityobj = new personentity()
                {
                    custentityobj = new customerentity()
                    {
                        custid = 1,
                        firstname = "boby",
                        lastname="singh",
                        city= "pune",
                        state="maharashtra"
                    },
                    empentityobj=new empentity()
                    {
                        empid=1,
                        emailid= "bobysingh",
                        password="12345"
                    }

                };

                var result = await new Persondal().Insert(personentityobj);

                Assert.IsNotNull(result);

            }).Wait();
        }
    }
}
